# Ansible: Deploy Hashicorp Vault!

[![Status](https://img.shields.io/badge/status-complete-brightgreen.svg?style=plastic)](#)
[![Platform](https://img.shields.io/badge/platform-CentOS%206-lightgrey.svg?style=plastic)](#)
[![GPLv3](https://img.shields.io/badge/license-GPLv3-blue.svg?style=plastic)](https://www.gnu.org/licenses/gpl.html)

This is an Ansible playbook that deploys [Hashicorp Vault](https://www.hashicorp.com/blog/vault.html) on a CentOS 6 server as part of the ezLSM capstone project. This playbook should be run first before any of the others. 

# Prerequisites

Ansible v2.0 is required to use this repo. To install Ansible, see [here](https://docs.ansible.com/ansible/intro_installation.html#installing-the-control-machine).

# Roles

This playbook consists of multiple roles:

1. common
    - installs basic packages required by later stages of the playbook
2. rsyslog
    - configures rsyslog to redirect security logs for comsumption by a [Splunk forwarder](https://www.splunk.com/en_us/download/universal-forwarder.html)
3. iptables
    - configures host-based firewall rules
4. auditd
    - configures the audit subsystem
5. psad
    - installs and configures [psad](http://cipherdyne.org/psad/) to detect port scan and other attacks
6. vault
    - installs and configures [Hashicorp Vault](https://www.hashicorp.com/blog/vault.html) for use as a central PKI server
7. splunkforwarder
    - installs and configures a [Splunk forwarder](https://www.splunk.com/en_us/download/universal-forwarder.html) to collect local logs and send them to a Splunk server
8. monit
    - installs and configures [monit](https://www.mmonit.com/monit/) for automated process monitoring

# Usage

## Deploying the playbook

1. clone this repo: 

    `$ git clone https://gitlab.com/ezLSM/deploy-vault-server.git`

2. add the hostname or IP address of the remote server(s) to the file `inventory`
3. configure options in `group_vars/all` 
4. run the playbook as the root user: 

    `$ ansible-playbook site.yml -i inventory -u root -k -K`
    
## Expected Outcome

See build status for expected outcome of a successful run of this playbook. 
