#!/usr/bin/python
# -*- coding: utf-8 -*-

# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

#---- Documentation Start ----------------------------------------------------#

DOCUMENTATION = '''
---
module: sign_csr
version_added: 2.1
author: "Ali Ibrahim"
requires: [ openssl ]
short_description: Signs a certificate signing request using a basic CA.
description:
     - The M(sign_csr) module is a wrapper around the openssl x509 command. It can be used to sign an RSA or ECDSA certificate signing request (CSR) using a basic Certificate Authority (CA). 
options:
  src:
    description:
      - Remote absolute path to the certificate signing request file.  
    required: true
    default: null
    aliases: ['csr']   
  dest:
    description:
      - Remote absolute path where the signed certificate file should be created. If you specify an extension, it is stripped off and replaced with .crt. If the parent directories do not exist, they are automatically created as long as the user has sufficient privileges.  
    required: true
    default: null
    aliases: ['path','destfile']   
  ca_cert:
    description:
      - Remote absolute path to the Certificate Authority's certificate file.  
    required: true
    default: null
  ca_key:
    description:
      - Remote absolute path to the Certificate Authority's private key file.  
    required: true
    default: null
  backup:
    description:
      - Creates a backup file including the timestamp information so you can get the original file back if you somehow clobbered it incorrectly. If set to C(no) and SSL certificate file already exists, it will do nothing. 
    required: false
    default: "no"
    choices: [ "yes", "no" ]    
  digest:
    description:
      - the message digest to use. 
    required: false
    default: null
    aliases: ['hash']
  days_valid:
    description:
      - the number of days the self-signed certificate should be valid for. 
    required: false
    default: "365"
  extfile:
    description:
      - Remote absolute path to a custom openssl configuration file to use when signing certificates. The openssl.cnf should contain valid syntax. 
    required: false
    default: null
    aliases: ['config', 'config_file']
  extensions:
    description:
      - This option is used alongside C(extfile) to allow specifying alternative sections to use when generating certificates. If the specified section does not exist in the config file, it will cause the task to fail. 
    required: false
    default: null
    aliases: ['ext']
  owner:
    description:
      - the owner id of the certificate and private key files, after execution. 
    required: false
    default: null
  group:
    description:
      - the group id of the certificate and private key files, after execution. 
    required: false
    default: null
  mode:
    description:
      - the permissions of the certificate file, after execution. The permissions of the private key is always set to 0600. 
    required: false
    default: null
notes:
  - "This module depends on I(openssl), which needs to be installed on all target systems."
'''

EXAMPLES = '''
# this section to be completed
'''

RETURN = '''
# this section to be completed
ssl_cert:
    description: the remote absolute path to the signed ssl certificate file
    returned: changed
    type: string
    sample: "/path/to/server.crt"
openssl_error:
    description: the error message returned by openssl
    returned: failed
    type: string
    sample: "problems making Certificate Request..."
'''

#---- Logic Start ------------------------------------------------------------#

# function to validate remote target path
def validate_destination(module):
    dest = os.path.expanduser(module.params['dest'])
    dir_name = os.path.dirname(dest)
    if not os.path.exists(dir_name):
        try:
            os.stat(dir_name)
        except OSError, e:
            if "permission denied" in str(e).lower():
                module.fail_json(msg="Destination directory %s is not accessible." % (dir_name))
        try:
            os.makedirs(dir_name, 0700)
        except OSError, e:
            if "permission denied" in str(e).lower():
                module.fail_json(msg="You don't have permission to create destination directory: %s." % (dir_name))
    if not os.access(os.path.dirname(dest), os.W_OK):
        module.fail_json(msg="Destination %s is not writable." % (dir_name))    


# function to validate that source files exist
def validate_file_exists(module, filename):
    if not os.path.exists(filename):
        module.fail_json(changed = False, msg="File %s does not exist." % filename)
  

# function to check for and backup existing ssl certificate
def backup_ssl_cert(module, filename):
    backup = module.params.get('backup', False)
    if backup:
        if os.path.exists(filename):
            module.backup_local(filename)
    else:
        if os.path.exists(filename):
            module.exit_json(changed=False, msg="SSL certificate file already exist.")


# function to check if chosen hash algorithm is valid
def check_hash_algorithm(module, openssl_binary, digest):
    openssl_cmd = shlex.split(openssl_binary + ' list-message-digest-algorithms')
    result, stdout, stderr = module.run_command(openssl_cmd)
    if result != 0:
        module.fail_json(changed = False, msg="Failed to execute openssl command: %s." % ' '.join(openssl_cmd), openssl_error = stderr)    
    supported_digests = [ i.lower() for i in stdout.split('\n') if "=>" not in i ] 
    if digest in supported_digests:
        return True
    else:
        module.fail_json(msg="Chosen hash algorithm %s is not supported on the target server." % digest)


def main():
    module = AnsibleModule(
        argument_spec = dict(
            src      = dict(required=True, type='path', aliases=["csr"]),
            dest      = dict(required=True, type='path', aliases=["path", "destfile"]),
            ca_cert   = dict(required=True, type='path'),
            ca_key    = dict(required=True, type='path'),
            backup    = dict(required=False, type='bool', default=False),
            digest    = dict(required=False, type='str', default=None, aliases=["hash"]),
            days_valid= dict(required=False, type='int', default=365),
            extfile  = dict(required=False, type='str', default=None, aliases=["config", "config_file"]),
            extensions  = dict(required=False, type='str', default=None, aliases=["ext"]),
            ),         
            add_file_common_args = True,
            supports_check_mode = False,
        )

    module.load_file_common_arguments(module.params)

    dest = os.path.expanduser(module.params['dest'])
    src = os.path.expanduser(module.params['src'])
    ca_cert = os.path.expanduser(module.params['ca_cert'])
    ca_key = os.path.expanduser(module.params['ca_key'])
    digest = module.params.get('digest', None)
    days_valid = module.params.get('days_valid', 365)
    config_file = module.params.get('extfile', None)
    extensions = module.params.get('extensions', None)
    
    mode   = module.params.get('mode', None)
    owner  = module.params.get('owner', None)
    group  = module.params.get('group', None)

    validate_file_exists(module, src)
    validate_file_exists(module, ca_cert)
    validate_file_exists(module, ca_key)
    validate_destination(module)
    
    cert_file = os.path.join(os.path.dirname(dest), os.path.splitext(os.path.basename(dest))[0] + '.crt')
    backup_ssl_cert(module, cert_file)

    openssl_binary = module.get_bin_path("openssl")
    if openssl_binary is None:
        module.fail_json(msg="This module requires OpenSSL which was not found on the system.")

    openssl_args = " x509 -req -in %s -out %s -CA %s -CAkey %s -CAcreateserial -days %s" % (src, cert_file, ca_cert, ca_key, days_valid)

    openssl_cmd = shlex.split(openssl_binary + openssl_args)

    if config_file is not None:
        if os.path.exists(config_file):
            openssl_cmd.append('-extfile')
            openssl_cmd.append(config_file)
            if extensions is not None:
                openssl_cmd.append('-extensions')
                openssl_cmd.append(extensions)
        else:
            module.fail_json(msg="Specified openssl.cnf file %s does not exist." % config_file)

    if digest is not None:
        digest = digest.lower()
        if check_hash_algorithm(module, openssl_binary, digest):
            openssl_cmd.append('-%s' % digest)

    result, stdout, stderr = module.run_command(openssl_cmd)

    if result != 0:
        module.fail_json(changed = False, msg="Failed to sign CSR.", openssl_error = stderr)
    else:
        if mode is not None:
            module.set_mode_if_different(cert_file, mode, False)
        if owner is not None:
            module.set_owner_if_different(cert_file, owner, False)
        if group is not None:
                module.set_group_if_different(cert_file, group, False)
        
        module.exit_json(changed = True, result = "SSL certificate %s signed successfully." % cert_file, ssl_cert=cert_file)


#---- Import Ansible Utilities (Ansible Framework) ---------------------------#

from ansible.module_utils.basic import *

if __name__ == '__main__':
    main()
