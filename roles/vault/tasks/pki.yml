---
- name: mount the PKI backend
  shell: /usr/local/bin/vault mount pki > /tmp/vault-pki.out
  args:
    creates: /tmp/vault-pki.out

- name: create combined root CA cert/key file 
  assemble: 
    src: /etc/vault/ssl
    dest: /etc/vault/ssl/rootCA.pem
    regexp: 'rootCA\.((key)|(crt))'
    owner: root
    mode: 0400

- name: import the root CA into Vault
  command: /usr/local/bin/vault write pki/config/ca pem_bundle=@/etc/vault/ssl/rootCA.pem

- name: delete the root CA from disk
  file: 
    path: "/etc/vault/ssl/{{ item }}"
    state: absent
  with_items:
    - rootCA.crt
    - rootCA.key
    - rootCA.pem
    - rootCA.srl
    - vaultserver.csr
    - openssl.cnf

- name: tune the maximum TTL time for generated/signed certificates
  command: "/usr/local/bin/vault mount-tune -max-lease-ttl={{ vault_pki_max_lease_time }}h pki" 

- name: configure the URL location for CRLs
  command: '/usr/local/bin/vault write pki/config/urls issuing_certificates="{{ vault_issuing_certificates_url }}" crl_distribution_points="{{ vault_crl_url }}"' 

- name: create pki roles
  command: "/usr/local/bin/vault write pki/roles/{{ item.name }} key_type={{ item.type }} key_bits={{ item.bits }} allowed_domains={{ item.domains }} {{ item.additional_options }}"
  with_items: "{{ vault_pki_roles }}"

- name: create policies for signing CSRs
  command: "/usr/local/bin/vault policy-write {{ item.policy_name }} /etc/vault/policies/{{ item.policy_filename }}"
  with_items: "{{ vault_policies }}"

- name: generate associated tokens for the policies created
  shell: "/usr/local/bin/vault token-create -display-name={{ item.associated_token_name }} -policy={{ item.policy_name }} -use-limit={{ item.token_use_limit }} -no-default-policy -format=yaml >> /root/.{{ item.associated_token_name }}.yml" 
  args:
    creates: "/root/.{{ item.associated_token_name }}.yml"
  with_items: "{{ vault_policies }}"
