
path "pki/sign/server-rsa" {
    capabilities = ["read", "update"]
}

path "pki/sign/server-ecdsa" {
    capabilities = ["read", "update"]
}
