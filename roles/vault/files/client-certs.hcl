
path "pki/sign/client-rsa" {
    capabilities = ["read", "update"]
}

path "pki/sign/client-ecdsa" {
    capabilities = ["read", "update"]
}
