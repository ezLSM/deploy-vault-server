---

#########################################################################
### Vault settings
#########################################################################

# the local user running the playbook
local_user: "{{ lookup('env','USER') }}"


# Vault policies
vault_policies:
  - policy_filename: server-certs.hcl
    policy_name: server-certs-policy
    associated_token_name: server-certs-token
    token_use_limit: "{{ server_token_use_limit|default(6) }}"

  - policy_filename: client-certs.hcl
    policy_name: client-certs-policy
    associated_token_name: client-certs-token
    token_use_limit: "{{ client_token_use_limit|default(100) }}"


# Vault PKI roles
vault_pki_roles:
  - name: "server-rsa"
    type: "rsa"
    bits: "2048"
    domains: "{{ vault_pki_permitted_domains | join(',') }}"
    additional_options: "client_flag=false allow_subdomains=true"

  - name: "server-ecdsa"
    type: "ec"
    bits: "384"
    domains: "{{ vault_pki_permitted_domains | join(',') }}"
    additional_options: "client_flag=false allow_subdomains=true"

  - name: "client-rsa"
    type: "rsa"
    bits: "2048"
    domains: "{{ vault_pki_permitted_domains | join(',') }}"
    additional_options: "server_flag=false allow_subdomains=true"

  - name: "client-ecdsa"
    type: "ec"
    bits: "384"
    domains: "{{ vault_pki_permitted_domains | join(',') }}"
    additional_options: "server_flag=false allow_subdomains=true"


# Vault localhost URLs
vault_root_cert_url: "https://127.0.0.1:8200/v1/pki/ca/pem"
vault_ecdsa_cert_url: "https://127.0.0.1:8200/v1/pki/sign/client-ecdsa"
vault_rsa_cert_url: "https://127.0.0.1:8200/v1/pki/sign/client-rsa"


# do not modify these variables
first_key: "{{ vault_init_output.stdout_lines[0]| default(False) | regex_replace('^Key\\s\\d:\\s(.*)$', '\\1') }}"

second_key: "{{ vault_init_output.stdout_lines[1]| default(False) | regex_replace('^Key\\s\\d:\\s(.*)$', '\\1') }}"

third_key: "{{ vault_init_output.stdout_lines[2]| default(False) | regex_replace('^Key\\s\\d:\\s(.*)$', '\\1') }}"

vault_root_token: "{{ vault_init_output.stdout_lines[5] | regex_replace('^Initial\\sRoot\\sToken:\\s(.*)$', '\\1') }}"
